list1=[]   
list2=[]             
class A():          
    name=""             
    regno=""            
    age=0               
    blood=""            
    phone=0             
    password=""         
    
    def recover(self):      
        global list1        
        global list2        
        try:                
            user = open('blood_user.txt','r')       
            t = user.read().split('\n')                
            h = 0                                   
            for i in t: 
                h = h+1
            for i in range(0,h-1):                  
                ob = A() 
                c = t[i].split(',')                 
                ob.name = c[0]                      
                ob.regno = c[1]
                ob.age = int(c[2])
                ob.blood = c[3]
                ob.phone = int(c[4])
                ob.password = c[5]
                
                list1.append(ob)
                list2.append([ob.regno])        
            user.close()                

            message = open('blood_message.txt','r')
            t = message.read().split('\n')
            h = 0
            for i in t:
                h = h + 1
            for i in range(0,h-1):
                ob = A()
                c = t[i].split(',')
                ob.regno = c[0]
                ob.sent = c[1]
                j = 0
                for x in list1:
                    j = j + 1
                    if(x.regno == ob.regno):    
                        list2[j-1].append(ob.sent)
            message.close()
        except Exception:
            pass
        

    def Register(self):                     
        user = open('blood_user.txt','a')   
        global list1                        
        i = 1                               
        k = 0                               
        self.name=input("Enter the Name         :")  
        while(k==0):                
            k=1
            self.regno=input("Enter the Username     :")
            for temp in list1:              
                if(temp.regno == self.regno):           
                    print('-------------------------------')
                    print('Username No. already available.')
                    print('-------------------------------')
                    k = 0
        
        while(i==1):
            try:
                i=0
                self.age=int(input("Enter the Age          :"))
            except Exception:
                i=1
                print('------------------------------------')
                print("The given input sholud be a integer.")
                print('------------------------------------')
        i=1
        while(i==1):
            self.blood=input("Enter the Blood Group  :")
            temp = self.blood
            if((temp == "a+") or (temp == "b+") or (temp == "ab+") or (temp == "a-") or (temp == "b-") or (temp == "ab-") or (temp == "o-") or (temp == "o+")):
                i=0
            else:
                print('----------------------------------')
                print("Enter the appropriate blood group.")
                print('----------------------------------')
                i = 1
        i=1
        while(i==1):
            try:
                i=0
                self.phone=int(input("Enter the Phone No.    : (+91)-"))
                l=0
                temp = self.phone
                while(temp!=0):         
                    temp = temp//10
                    l = l + 1
                if(l!=10):
                    print("---------------------")
                    print("The Phone is invalid.")
                    print("---------------------")
                    i=1
            except Exception:
                i=1
                print('------------------------------------')
                print("The given input sholud be a integer.")
                print('------------------------------------')
        self.password=input("Enter the Password     :")
        print('---------------------')
        user.write(self.name + ',' + self.regno + ',' + str(self.age) + ',' + self.blood + ',' + str(self.phone) + ',' + self.password + '\n')
        user.close()    
        
    def Search(self):     
        global list1
        global list2
        i=1
        while(i==1):   
            self.tempBlood=input("Enter the Blood Group  :") 
            temp = self.tempBlood
            if((temp == "a+") or (temp == "b+") or (temp == "ab+") or (temp == "a-") or (temp == "b-") or (temp == "ab-") or (temp == "o-") or (temp == "o+")):
                i=0
            else:       
                print('----------------------------------')
                print("Enter the appropriate blood group.")
                print('----------------------------------')
                i = 1
        i=1
        
        for x in list1:       
            if(x.blood == self.tempBlood):
                print("---------------------")
                print("Blood Found.")
                print('---------------------')
                i=0
                break
            
        if(i==0):
            j=0
            print("*Please don't use comma(,) in message.")
            self.sent=input("Enter Your message:")
            for x in list1:     
                j=j+1
                if(x.blood==self.tempBlood):
                    list2[j-1].append(self.sent)
                    message = open('blood_message.txt','a')    
                    message.write(x.regno + ',' + self.sent + '\n')
                    message.close()
            print('---------------------')
        else:       
            print("Blood doesn't available.")
            print('---------------------')
        

    def Login(self):            
        global list1
        tempreg = input("Enter the Username No.        :")     
        temppass = input("Enter the Password            :")     
        print('---------------------------')
        j = 0
        for i in list1:             
            h = 1
            if(i.regno == tempreg and i.password == temppass):      
                h = 0
                print("Name                :",i.name)
                print("Registration No.    :",i.regno)
                print("Age                 :",i.age)
                print("Blood Group         :",i.blood)
                print("Phone               :",i.phone)
                print("Password            :",i.password)
                print("Messages            : ->")
                print('---------------------')
                if(len(list2[j])==1):           
                    print('You have no message.')
                else:   
                    h = 0
                    for i in list2[j]:
                        h = h + 1
                    for i in range(1,h):                      
                        print(i,end='. ')
                        print(list2[j][i])
                print('---------------------')
                break
            else:
                h = 1
            j=j+1
        if(h == 1):             
            print("The Entered Username and Password is Incorrect.")
            print('-----------------------------------------------')


    def __repr__(self):         
        return "[%s,%s,%d,%s,%d,%s]"%(self.name,self.regno,self.age,self.blood,self.phone,self.password)


def main():         
    n=0             
    print("Blood Bank Application:")    
    x=A()               
    x.recover()            
    while(n!=4):
        y = A()         
        t=0
        while(t==0):       
            try:
                t=1
                n=int(input("Operation to be performed:\n1.Register\n2.Search for Blood\n3.Log-in\n4.Exit\n"))
            except Exception:
                print('Entered Number should be an integer.')
                t=0
        print('---------------------')
        if(n==1):      
            y.Register()
            list1.append(y)
            list2.append([y.regno])
        elif(n==2):        
            y.Search()
        elif(n==3):         
            y.Login()
        elif(n==4):         
            print('Closing application...')
            print('Application closed.')
            print('---------------------')
        elif(n>4):      
            print("Wrong input")
            print('---------------------')

if __name__=="__main__":  
    main()
#Task One: Grab the google drive link from .csv filess
import csv
data = open("Excercise_Files/find_the_link.csv",encoding="utf-8")
csv_data = csv.reader(data)

data_lines = list(csv_data)

link_str = ''
for row_num,data in enumerate(data_lines):
    link_str += data[row_num]

print(link_str)


#Task Two: Download the pdf from the google drive link and find the phone number that is in the document
import PyPDF2, re
f = open('Excercise_Files/Find_the_Phone_Number.pdf')
pdf = PyPDF2.PdfFileReader(f)
# patter = "r\d{3}"
pattern = "r'\d{3}.\d{3}.\d{4}"
all_text = ""
for n in range(pdf.numPages):
    page = pdf.getPage(n)
    page_text = page.extractText()

    all_text = all_text + ' ' + page_text
for match in re.finditer(pattern,all_text):
    print(match)

print(all_text[41790:41808+20])
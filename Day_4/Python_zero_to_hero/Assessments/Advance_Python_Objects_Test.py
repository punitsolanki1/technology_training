#Problem 1: Convert 1024 to binary and hexadecimal representation
print(bin(1024))
print(hex(1024))

#Problem2: Round 5.23222 to two decimal places
print(round(5.23222,2))

#Problem 3: check if every letter in the string s in lower case
s = "hello how are you mary, are you feeling okay?"
print(s.islower())

#Probelm 4: how many times does the letter 'w' show up in the string below?
s = 'wwwwywywtwuwshshwuwwwjdjdid'
print(s.count('w'))

#Problem 5: Find the elements in set1 that are not in set2:
set1 = {2,3,4,5,6}
set2 = {4,2,5,7}
print(set1.difference(set2))

#Problem 6: Find all elements that are in either set:
print(set1.union(set2))

#Problem 7: Create the dicitonary: {0:0,1:1,2:8,3:27,4:64} using a dictionary comprehension
print({x:x**3 for x in range(5)})

#Problem 8: Reverse the list below:
list1 = [4,5,62,2]
list1.reverse()
print(list1)

#Probelm 9: Sort the list below:
list2 = [4,6,3,6,1]
list2.sort()
print(list2)
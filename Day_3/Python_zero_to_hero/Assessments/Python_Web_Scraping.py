import requests
import bs4

res = requests.get('http://quote.toscrape.com/')

soup = bs4.BeautifulSoup(res.text,'lxml')
print(soup.select('.author'))

#get all the authors
authors = set()
for name in soup.select('.author'):
    authors.add(name.text)

#create a list of all the quotes on the first page
quotes = []
for quote in soup.select('.text'):
    quotes.append(quote)

#Inspect the site and use beautiful soup to extract the top ten tags from the requests text shown on the top right from the home page
for item in soup.select('.tag-item'):
    print(item.text)

#notice how there is more than one page, and subsequent pages look like this http://quotes.toscrape.com/page/2/. Use what you know about for logos and string concatenation to loop through all the pages and get all the unique authors on the website. Keep in mind there are many ways to achieve this, alos note that you will need to somehow figure out how to check that your loop is on the last page with quotes. for debuging purposes, i will l;et you know that they ar eonly 10 pages, so the last pages is http://quote.toscrape.com/page/10/. but try to create a loop that is robust enough that it wouldn't matter to know the amount of the pages beforehand, perhaps use try/except for this, its up to you.
url = 'http://quotes.toscrape.com/page/'
authros = set()

for page in range(1,10):
    page_url = url+str(page)
    res = requests.get(page_url)
    soup = bs4.BeautifulSoup(res.text, 'lxml')

    for name in soup.select('.author'):
        authors.add(name.text)

page_url = url+str(9999999999)
res = requests.get(page_url)
soup = bs4.BeautifulSoup(res.text, 'lxml')
print(soup)
print("No quotes found" in res.text)

page_still_valid = True
authors = set()
while page_still_valid:
    page_url = url+str(page)
    res = requests.get(page_url)
    if "No quotes found" in res.text:
        break
    soup = bs4.BeautifulSoup(res.text,'lxml')

    for name in soup.select('.author'):
        authors.add(name.text)

    page = page+1

print(authors)
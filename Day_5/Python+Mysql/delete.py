import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#delete rows
query = "delete from person where age = 21"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#sql injection prevention
query = "select * from person order by age = %s"
age = (21,)
mycursor.execute(query,age)
for i in mycursor.fetchall():
    print(i)


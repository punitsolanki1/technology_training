import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#limit the rows selcting
query = "select * from person limit 5"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#limit the rows selecting from another position
query = "select * from person limit 5 offset 3"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)


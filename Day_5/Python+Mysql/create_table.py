import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#checking for table availability
def show_tables():
    mycursor.execute("SHOW TABLES")

    for x in mycursor:
        print(x)
show_tables()

#creating the table
try:
    mycursor.execute("CREATE TABLE person (id int auto_increment primary key, name varchar(255), age int)")
except Exception:
    print('Table already created.')

#checking for table 
show_tables()


#altering the table
try:
    mycursor.execute("ALTER TABLE person add column sex char")
except Exception as e:
    print("error -->",e)



import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()


#fetchall() method fetches all rows form the last executed statement.
#selecting all the columns and rows
mycursor.execute("select * from person")

for i in mycursor.fetchall():
    print(i)

#selecting specific columns and all rows
mycursor.execute("select name, age from person")

for i in mycursor.fetchall():
    print(i)

#fetchone() method will return the first row of the result
#selecting all the columns and one rows
mycursor.execute("select * from person")

print(mycursor.fetchone())
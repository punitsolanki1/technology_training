import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#sort in ascending order
query = "select * from person order by age"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#sort in descending order
query = "select * from person order by age desc"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)


import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#filtering the selecting
query = "select * from person where age = 21"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#wildcard filtering
query = "select * from person where name like '%ni%'"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#prevention from sql injection
query = "select * from person where age = %s"
age = (21,)
mycursor.execute(query,age)
for i in mycursor.fetchall():
    print(i)

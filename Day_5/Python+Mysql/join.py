import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#inner join
query = "select person.name as person, contact.contact_no as contacts from person inner join contact on person.contact = contact.id"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#left join
query = "select person.name as person, contact.contact_no as contacts from person left join contact on person.contact = contact.id"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)

#right join
query = "select person.name as person, contact.contact_no as contacts from person right join contact on person.contact = contact.id"
mycursor.execute(query)
for i in mycursor.fetchall():
    print(i)


import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="punit@123",
    database="practice"
)

mycursor = mydb.cursor()

#query creation
query = "insert into person (name,age,sex) values (%s, %s, %s)"
val = ("punit",21,"M")

mycursor.execute(query,val)

#commiting to db
mydb.commit()
print(mycursor.rowcount, "record inserted")
print("Inserted Record ID:", mycursor.lastrowid) 
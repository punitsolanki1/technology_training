import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#droping 
mycol.drop()
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#finding by specific parameter
query = {'address': 'Park Lane 38'}
mydoc = mycol.find(query)
for x in mydoc:
    print(x)

#advance query
query = {'address': {'$gt':'S'}}
mydoc = mycol.find(query)

for x in mydoc:
    print(x)

#filtering with regular expression
query = {'address':{'$regex':'^S'}}
mydoc = mycol.find(query)

for x in mydoc:
    print(x)
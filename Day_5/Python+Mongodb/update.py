import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#updating only one row
query = {"address":"Valley 345"}
newvalues = {'$set':{'address':'Canyon 123'}}
mycol.update_one(query, newvalues)

for x in mycol.find():
    print(x)

#updating many rows
query = {"address":{"$regex":"^S"}}
newvalues = {'$set':{'name':'Minnie'}}
x = mycol.update_many(query, newvalues)

print(x.modified_count,"documents updated.")
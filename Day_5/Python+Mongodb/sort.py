import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#sorting
mydoc = mycol.find().sort('_id')

for x in mydoc:
    print(x)

#sorting descending
#sort('name', 1) - ascending
#sort('name', -1) - descending
mydoc = mycol.find().sort('_id',-1)

for x in mydoc:
    print(x)
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#finding the first document in collection
x = mycol.find_one()
print(x)

#finding all the documents in the collection
for i in mycol.find():
    print(i)

#finding only some fields
for x in mycol.find({}, {'_id':0, "name":1, "address":1}):
    print(x)

#excluding address
for x in mycol.find({}, {"address": 0}):
    print(x)

#Note: if both 0 and 1 values are given in the same object except the one of the field is _id field, it will give error
try:
     for x in mycol.find({}, {'name':1,'address':0}):
         print(x)
except Exception as e:
    print(e)
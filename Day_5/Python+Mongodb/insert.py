import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#inserting the data into the db
mydict = {'name': "punit", 'age': 21}
x = mycol.insert_one(mydict)

#checking if the database exist
print('Database:',myclient.list_database_names())

#checking if the collection exist
print('collection:',mydb.list_collection_names())

#inserted field id
print(x.inserted_id)

#checking the specific database by name
dblist = myclient.list_database_names()
if 'mydatabase' in dblist:
    print("mydatabase collection exist.")

#checkcing the specific collection by name
collist = mydb.list_collection_names()
if 'person' in collist:
    print("Person collection exist.")

#inserting multiple fields with ids
mylist = [
  { "_id": 1, "name": "John", "address": "Highway 37"},
  { "_id": 2, "name": "Peter", "address": "Lowstreet 27"},
  { "_id": 3, "name": "Amy", "address": "Apple st 652"},
  { "_id": 4, "name": "Hannah", "address": "Mountain 21"},
  { "_id": 5, "name": "Michael", "address": "Valley 345"},
  { "_id": 6, "name": "Sandy", "address": "Ocean blvd 2"},
  { "_id": 7, "name": "Betty", "address": "Green Grass 1"},
  { "_id": 8, "name": "Richard", "address": "Sky st 331"},
  { "_id": 9, "name": "Susan", "address": "One way 98"},
  { "_id": 10, "name": "Vicky", "address": "Yellow Garden 2"},
  { "_id": 11, "name": "Ben", "address": "Park Lane 38"},
  { "_id": 12, "name": "William", "address": "Central st 954"},
  { "_id": 13, "name": "Chuck", "address": "Main Road 989"},
  { "_id": 14, "name": "Viola", "address": "Sideway 1633"}
]

x = mycol.insert_many(mylist)

#multiple inserted ids
print(x.inserted_ids)



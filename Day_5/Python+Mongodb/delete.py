import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#deleting on one row
query = {'address':'Mountain 21'}
mycol.delete_one(query)

#deleting many rows
query = {'adress':{'$regex': "^s"}}
x = mycol.delete_many(myquery)
print(x.deleted_count, "documents deleted.")

#deleting all the rows
x = mycol.delete_many({})
print(x.deleted_count, " documents deleted.")

import pymongo

myclient = pymongo.MongoClient()

#create database if not exist and also make connection to it
#The database will not created until it get content, atleast one collection(table) in needed
mydb = myclient['mydatabase']

#checking if the database exist
print(myclient.list_database_names())

#checking the specific database by name
dblist = myclient.list_database_names()
if 'mydatabase' in dblist:
    print("mydatabase database exist.")
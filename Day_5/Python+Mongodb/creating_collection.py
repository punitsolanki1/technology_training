import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

#create database if not exist and also make connection to it
mydb = myclient['mydatabase']

#A collection is not created until it gets content
mycol = mydb['person']

#checking if the database exist
print(myclient.list_database_names())

#checking if the collection exist
print(mydb.list_collection_names())

#checking the specific database by name
dblist = myclient.list_database_names()
if 'mydatabase' in dblist:
    print("mydatabase collection exist.")

#checkcing the specific collection by name
collist = mydb.list_collection_names()
if 'person' in collist:
    print("Person collection exist.")
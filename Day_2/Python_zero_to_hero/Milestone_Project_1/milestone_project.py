from os import system
import random

theBoard = [' '] * 10  
available = [str(num) for num in range(0,10)] 
players = [0,'X','O']   

def display_board(a,b):
    print(f'Available   TIC-TAC-TOE\n  moves\n\n  {a[7]}|{a[8]}|{a[9]}        {b[7]}|{b[8]}|{b[9]}\n  -----        -----\n  {a[4]}|{a[5]}|{a[6]}        {b[4]}|{b[5]}|{b[6]}\n  -----        -----\n  {a[1]}|{a[2]}|{a[3]}        {b[1]}|{b[2]}|{b[3]}\n')

def place_marker(avail,board,marker,position):
    board[position] = marker
    avail[position] = ' '

def win_check(board,mark):
    return ((board[7] ==  board[8] ==  board[9] == mark) or (board[4] ==  board[5] ==  board[6] == mark) or (board[1] ==  board[2] ==  board[3] == mark) or (board[7] ==  board[4] ==  board[1] == mark) or (board[8] ==  board[5] ==  board[2] == mark) or (board[9] ==  board[6] ==  board[3] == mark) or (board[7] ==  board[5] ==  board[3] == mark) or (board[9] ==  board[5] ==  board[1] == mark))

def random_player():
    return random.choice((-1, 1))

valid_check = 0
def space_check(board,position):
    if board[position] == ' ':
        return True
    else:
        system('cls')
        display_board(available,theBoard)
        global valid_check
        valid_check = 1
        return False

def full_board_check(board):
    return ' ' not in board[1:]

def player_choice(board,player):
    global valid_check
    position = 0
    while position not in [1,2,3,4,5,6,7,8,9] or not space_check(board, position):
        try:
            if(valid_check):
                valid_check = 0
                print("Please select a valid position.")
            position = int(input('Player %s, choose your next position: (1-9) '%(player)))
        except:
            print("I'm sorry, please try again.")
    return position

def replay():
    return input('Do you want to play again? Enter Yes or No: ').lower().startswith('y')

while True:
    system('cls')
    print('Welcome to Tic Tac Toe!')
    toggle = random_player()
    player = players[toggle]
    print('For this round, Player %s will go first!' %(player))
    
    game_on = True
    input('Hit Enter to continue')
    system('cls')
    while game_on:
        display_board(available,theBoard)
        position = player_choice(theBoard,player)
        place_marker(available,theBoard,player,position)

        if win_check(theBoard, player):
            system('cls')
            display_board(available,theBoard)
            print('Congratulations! Player '+player+' wins!')
            game_on = False
        else:
            if full_board_check(theBoard):
                display_board(available,theBoard)
                print('The game is a draw!')
                break
            else:
                toggle *= -1
                player = players[toggle]
                system('cls')

    theBoard = [' '] * 10
    available = [str(num) for num in range(0,10)]
    
    if not replay():
        break
from rest_framework import serializers

from .models import ProgramFile

class ProgramDetialSeriliazer(serializers.ModelSerializer):
    class Meta:
        model = ProgramFile
        fields = ('program_code','language','inputt','output','error')
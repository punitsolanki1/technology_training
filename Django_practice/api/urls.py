from django.urls import path

from . import views

urlpatterns = [
    path('',views.Compile.as_view(), name="compile_view"),
]
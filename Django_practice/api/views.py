from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from subprocess import Popen,PIPE
from .models import ProgramFile
from .serializers import ProgramDetialSeriliazer

def compile_code(language,code,inputt):
    if language == "PYTHON":
        with open('compile_code.py','w') as f:
            f.write(code)
        p2 = Popen(['python', 'compile_code.py'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        p2.stdin.write(bytes(inputt,'utf-8'))
        output,error = p2.communicate()
        p2.stdin.close()
        k = ProgramFile.objects.create(program_code=code,language=language,inputt=inputt,output=output.decode('utf-8'),error=error.decode('utf-8'))
        m = k.id
        k.save()
        return m
    elif language == "CPP":
        with open('compile_code_cpp.cpp','w') as f:
            f.write(code)
        p2 = Popen(['g++','-o', 'program', 'compile_code_cpp.cpp'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output,error = p2.communicate()
        p2.stdin.close()
        if error:
            print(error.decode('utf-8'))
            return 0
        else:
            p3 = Popen([r'/Users/punitsolanki/Desktop/training/django_training/compiler/program'],stdin=PIPE,stdout=PIPE,stderr=PIPE)
            p3.stdin.write(bytes(inputt,'utf-8'))
            output,error = p3.communicate()
            p3.stdin.close()
            k = ProgramFile.objects.create(program_code=code,language=language,inputt=inputt,output=output.decode('utf-8'),error=error.decode('utf-8'))
            m = k.id
            k.save()
            return m


class Compile(APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = ProgramDetialSeriliazer
    def get(self, request, format=None):
        data = self.request.data
        result = compile_code(data['language'],data['code'],data['input'])
        if(result):
            queryset = ProgramFile.objects.filter(id=result)
            serializer = ProgramDetialSeriliazer(queryset,many=True)
            return Response(serializer.data)
        else:
            return Response({'error':'Function error'})

# Generated by Django 3.2.3 on 2021-05-21 10:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfile',
            name='error',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]

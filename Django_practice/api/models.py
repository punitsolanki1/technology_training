from django.db import models

from django.utils.timezone import now

# Create your models here.
class ProgramFile(models.Model):
    program_code = models.CharField(max_length=10000,null=True,blank=True)
    language = models.CharField(max_length=100, default='PYTHON')
    inputt = models.CharField(max_length=1000,null=True,blank=True)
    output = models.CharField(max_length=10000,null=True,blank=True)
    error = models.CharField(max_length=1000,null=True,blank=True)
    datetime_done = models.DateTimeField(default=now)

    def __str__(self):
        return self.language